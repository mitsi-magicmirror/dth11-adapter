'use strict';

function log(msg, lvl = "info"){
	if(lvl == "info") console.log("[MMM-DHT11]", msg);
	else console.error("[MMM-DHT11]", msg);
}

Module.register("MMM-dht11", {
	defaults: {
		updateInterval: 10000,
		pin: 24
	},
	
	start: function() {
		log("Start");
		console.log('Starting module: ', this.name, this.config);
		this.temperature = -1;
		this.humidity = -1;

		var self = this;
		this.loaded = false;

		this.getData();

		var nextLoad = this.config.updateInterval;
		var self = this;
		setInterval(function() {
		  self.getData();
		}, nextLoad);
	},
	
	getData: function() {
		console.log("getData", this.config.pin);
		this.sendSocketNotification('GET_TEMPERATURE', this.config.pin);
	},
	
	/*getStyles: function() {
		return [];
	},*/
	
	getDom: function() {
		console.log("getDom", this.config, this.temperature, this.humidity);
		var wrapper = document.createElement("p");
		wrapper.className = 'medium bright';
		wrapper.innerHTML = `${this.temperature}°C / ${this.humidity}%`;
		return wrapper;
	},
	
	socketNotificationReceived: function(notification, payload) {
		console.log("socketNotificationReceived", this.config.pin,notification, payload);
		if (notification === "RESULT_TEMPERATURE") {
			var self = this;
			this.temperature = payload.temperature;
			this.humidity = payload.humidity;
			this.updateDom();
		}
  },
});
