var NodeHelper = require('node_helper');
const sensor = require("node-dht-sensor");

function log(msg, lvl = "info"){
	if(lvl == "info") console.log("[MMM-DHT11]", msg);
	else console.error("[MMM-DHT11]", msg);
}

module.exports = NodeHelper.create({
  start: function () {
    log('Node Helper created, trying getting data...');
    this.getTemperature(24);
  },

  getTemperature: function (pin) {
      var self = this;
		sensor.read(11, pin, function(err, temperature, humidity) {
		  if (!err) {
		    log(`temp: ${temperature}°C, humidity: ${humidity}%`);
		    self.sendSocketNotification('RESULT_TEMPERATURE', {temperature, humidity});
		  }
		  else{
		  	log(err, "err");
		  }
		});
  },

  //Subclass socketNotificationReceived received.
  socketNotificationReceived: function(notification, payload) {
    if (notification === 'GET_TEMPERATURE') {
		console.log(notification);
		this.getTemperature(payload);
    }
  }

});
